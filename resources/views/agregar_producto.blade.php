<x-app-layout>



    @section('Titulo')
Artisen
@endsection





@section('contenido')


<div class="row justify-content-md-center">
    <div class="col-lg-5 d-flex justify-content-center ">
        <div class="px-lg-5 py-lg-4 p-4 align-self-center w-100">
        <h1 class="d-flex justify-content-center ">Registro Productos</h1>

<form class="mb-3" action="{{ route('productos.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label font-weight-bold">Nombre</label>    
    <input type="text" class="form-control bg-dark-x border-0" name="nombre" placeholder="Ingrese su producto" required/>
    </div>

    <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label font-weight-bold">Descripcion</label>    
    <input type="text" class="form-control bg-dark-x border-0" name="descripcion" placeholder="Ingrese descripcion" value="{{ old('descripcion') }}" required/>
    </div>

    <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label font-weight-bold">Categoria</label>        
    <input type="text" class="form-control bg-dark-x border-0" name="categoria" placeholder="Ingrese categoria" value="{{ old('categoria') }}" required/>
    </div>

    <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label font-weight-bold">Precio</label>   
    <input type="number" class="form-control bg-dark-x border-0" name="precio" placeholder="Ingrese el precio" value="{{ old('precio') }}" required/>
    </div>


    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label font-weight-bold">Cantidad</label>   
        <input type="number" class="form-control bg-dark-x border-0" name="cantidad" placeholder="Ingrese cantidad" value="{{ old('cantidad') }}" required/>
        </div>

    <div class="mb-3">
        <label for="formFile" class="form-label">Ingrese su foto</label>
        <input class="form-control" type="file" name="imagen" required>
        </div>

    <div class="d-flex justify-content-center">
    <button type="submit" class="btn btn-primary w-100 ">Enviar</button>
</div>
</form>


</div>

</div>
</div>
@endsection

</x-app-layout>