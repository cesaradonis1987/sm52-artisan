<x-app-layout>



    @section('Titulo')
Artisen
@endsection





@section('contenido')

<!-- Header -->
<div class="bg-dark p-3 mt-4"> 
  <h1 class="text-center m-0 text-light">Detalles</h1>
  </div>

  <!-- Photo Grid -->
      <!-- Row 1 -->
  <div class="card-deck mt-5">
      <div class="card mb-12">

        <div class="row g-0">

        <div class="col-md-4 d-flex justify-content-center">
        <img src="{{asset('storage/'.$producto->imagen)}}" class="ms-auto m-1" width="100%">
        </div>

        <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">DETALLES</h5>
          <ul class="list-group ">
            <li class="list-group-item list-group-item"><i class="fas fa-id-badge" style="font-size:20px;"></i> ID: {{ $producto->id}}</li>
            <li class="list-group-item list-group-item"><i class="fas fa-shopping-bag" style="font-size:20px;"></i> Producto: {{ $producto->nombre}}</li>
            <li class="list-group-item list-group-item"><i class="fas fa-id-card"style="font-size:20px;"></i> Descripcion: {{ $producto->descripcion}}</li>
            <li class="list-group-item list-group-item"><i class="fas fa-money-bill-alt"style="font-size:20px;"></i> Costo: ${{ $producto->precio}}</li>
            <li class="list-group-item list-group-item"><i class="fas fa-box"style="font-size:20px;"></i> Cantidad: {{$producto->cantidad}}</li>
          </ul>
          
        </div>
    
        <div class="card-footer">
          
          <a href="{{ route('productos.edit', $producto->id)}}" class="btn  btn-warning w-100"><i class="far fa-edit"></i></a>
        </div>
      </div>
      
      </div>
      </div>
  </div>
@endsection

</x-app-layout>