<x-app-layout>



    @section('Titulo')
Artisen
@endsection





@section('contenido')

<div class="bg-dark p-3 mt-4"> 
    <h1 class="text-center m-0 text-light">Lista de Productos</h1>
    </div>






    <a href="{{route('productos.create')}}" class="btn btn-success w-100 mt-5" >Agregar</a>

    @if(session()->has('mensaje'))
    <p style="color: rgb(14, 182, 56)">{{ session('mensaje') }}</p>
    @endif





    
    <table class="table  mt-2" id="data" class="display">
        <thead class="table-dark">
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Nombre</th>
              <th scope="col">Descripcion</th>
              <th scope="col">Categoria</th>
              <th scope="col">Precio</th>
              <th scope="col">Cantidad</th>
              <th scope="col">Foto</th>

              
              <th scope="col" class="text-center">Opciones</th>
              
            </tr>
          </thead>



        @foreach ($productos as $productos)
        <tbody>    
            <tr>
                <th scope="row">{{ $productos->id }}</th>


                <td>{{ $productos->nombre }}</td>
                <td>{{ $productos->descripcion }}</td>
                <td>{{ $productos->categoria }}</td>
                <td>{{ $productos->precio }}</td>
                <td>{{ $productos->cantidad }}</td>
                <td><img src="{{asset('storage/'.$productos->imagen)}}" 
                     alt="..." width="50px" height="50px"></td>
                
                    

                <td>
                    <div class="d-flex justify-content-center">
                <a href="{{ route('productos.show', $productos->id)}}" class="btn  btn-info active mx-1 d-inline "><i class="far fa-eye"></i></a>
                <a href="{{ route('productos.edit', $productos->id)}}" class="btn  btn-warning active mx-1 d-inline"><i class="far fa-edit"></i></a>
                
                    <form action="{{ route('productos.destroy', $productos->id) }}" method="post" class="d-inline mx-1">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger "><i class="fas fa-trash-alt"></i></button>
                    </form>
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
 

    
    

@endsection

</x-app-layout>