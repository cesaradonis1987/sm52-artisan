<x-app-layout>
    @section('Titulo')
Artisen
@endsection





@section('contenido')

<div class="bg-dark p-3 mt-4"> 
  <h1 class="text-center m-0 text-light">Acerca de</h1>
  </div>

    <div class="card mb-12 mt-5" >
        <div class="row g-0">
          <div class="col-md-5 d-flex justify-content-center">
            <img src="{{asset('adjuntos/QUIENESSOMOS.jpg')}}"  alt="..." class="img-fluid">
          </div>
          <div class="col-md-7">
            <div class="card-body ">
              <h5 class="card-title text-center">Artisen</h5>
              <p class="card-text text-justify"> Somos una empresa con una visión hacia un tipo de mercado en especial. Como buenos fanáticos de los temas de animación japonesa, nos complace realizar un trabajo para personas que comparten la misma pasión que nosotros. Nuestro objetivo es ofrecer una plataforma  de  la  cual  podamos  enorgullecernos  y  a  la  vez  disfrutar  de  su  creación mientras vemos cual es la aceptación en esta gran comunidad. Artisen es un proyecto hecho con empeño y cariño por parte de cada uno de los integrantes del equipo de desarrollo.  cada  apartado  trabajando  los  conocimientos  obtenidos  de  excelentes instructores  y  diseños  hechos  inspirados  el  la  cultura  "Friki"  que  con  orgullo pertenecemos. Una plataforma de hecha para fanáticos por fanáticos. Artisen!
              </p>
              <p class="card-text"><small class="text-muted">Ultima actualizacion hoy.</small></p>
            </div>
          </div>
        </div>
      </div>

      <div class="p-1 mt-3"> 
        <h1 class="text-center m-0 text-light"></h1>
        </div>

@endsection
       
</x-app-layout>