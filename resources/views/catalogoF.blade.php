<x-app-layout>
    @section('Titulo')
Artisen
@endsection





@section('contenido')
<div class="bg-dark p-3 mt-4"> 
<h1 class="text-center m-0 text-light">Figuras Anime</h1>
</div>
   



<!-- START SECTION STORE -->
<section class="store mt-5">
  <div class="container">
      <div class="items">
          <div class="row g-2 d-flex justify-content-center">

            
              @foreach ($productos as $productos)
              
              
                  

              <div class="card h-100 d-inline d-flex justify-content-center m-2" style="width: 16rem;" >
                <div class="item shadow">
                  <img src="{{asset('storage/'.$productos->imagen)}}" class="item-image " alt="..." width="250px" height="250px" >
                  <div class="card-body ">
                    <h5 style="display: none;"class="item-id text-center">{{$productos->id}} </h5>
                    <h5 class="item-title text-center">{{$productos->nombre}} </h5>
                    <h5 class="item-price text-center">${{$productos->precio}} </h5>
                    <p class="card-text">{{$productos->descripcion}}</p>


                    @if (Route::has('login'))
                    @auth
                    <button class="item-button btn btn-primary addToCart w-100">AÑADIR AL CARRITO</button>
                      @else
                        <a href="{{ route('login') }}" class="item-button btn btn-primary addToCart w-100">AÑADIR AL CARRITO</a>
                    @endauth
                    @endif
                    

                  </div>
                </div>
                </div>
                  
              @endforeach
                  

            


          </div>
      </div>
  </div>
</section>
<!-- END SECTION STORE -->
<!-- START SECTION SHOPPING CART -->
<section class="shopping-cart">
  <div class="container">
      <h1 class="text-center">CARRITO</h1>
      <hr>
      <div class="row">
          <div class="col-6 ">
              <div class="shopping-cart-header w-100">
                  <h6>Producto</h6>
              </div>
          </div>
          <div class="col-3">
              <div class="shopping-cart-header w-100">
                  <h6 class="text-truncate">Precio</h6>
              </div>
          </div>
          <div class="col-3">
              <div class="shopping-cart-header">
                  <h6>Cantidad</h6>
              </div>
          </div>
      </div>
      <!-- ? START SHOPPING CART ITEMS -->
      <div class="shopping-cart-items shoppingCartItemsContainer">
      </div>
      <!-- ? END SHOPPING CART ITEMS -->

      <!-- START TOTAL -->
      <div class="row">
          <div class="col-12">
              <div class="shopping-cart-total d-flex align-items-center">
                  <p class="mb-0">Total</p>
                  <p class="ml-4 mb-0 shoppingCartTotal">$0</p>
                  <div class="toast ml-auto bg-info" role="alert" aria-live="assertive" aria-atomic="true"
                      data-delay="2000">
                      <div class="toast-header">
                          <span>✅</span>
                          <strong class="mr-auto ml-1 text-secondary">Elemento en el carrito</strong>
                          <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div class="toast-body text-white">
                          Se aumentó correctamente la cantidad
                      </div>
                  </div>

                  
                  <form  action="{{ route('catalogoF.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id='foo0' name="nom" value=""/>
                    <input type="hidden" id='foo1' name="cant" value=""/>
                    <input type="hidden"  id='foo' name="cost" value=""/>
                    @if (Route::has('login'))
                    @auth
                    
                    <input type="hidden" name="idcli" value={{Auth::user()->id}}>
                    <input type="hidden" name="name" value={{Auth::user()->name}}>
                    @endauth
                    @endif
                    <input type="hidden" id='foo3'name="idpro" value=''>

                    @if (Route::has('login'))
                    @auth
                    <button class="btn btn-success ml-auto comprarButton" type="submit" data-toggle="modal"

                      data-target="#comprarModal">Comprar</button>
                      @else
                        <a href="{{ route('login') }}" class="btn btn-success ml-auto">Comprar</a>
                    @endauth
                    @endif
                    </form>


              </div>
          </div>
      </div>

      <!-- END TOTAL -->

      <!-- START MODAL COMPRA -->
      <div class="modal fade" id="comprarModal" tabindex="-1" aria-labelledby="comprarModalLabel"
          aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="comprarModalLabel">Gracias por su compra</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      <p>Pronto recibirá su pedido</p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                  </div>
              </div>
          </div>
      </div>
      <!-- END MODAL COMPRA -->


  </div>

</section>
<!-- END SECTION SHOPPING CART -->


<!-- START FOOTER -->
<footer class="bg-dark p-3 mt-5">
  <p class="text-center m-0 text-muted">Artisen</p>
</footer>
<!-- END FOOTER -->




<!-- SCRIPTS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
  integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
  crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
  integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
  crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
  integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
  crossorigin="anonymous"></script>

<script src="{{('js/tienda.js') }}"></script>






@endsection
       
</x-app-layout>