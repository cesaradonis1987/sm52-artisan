<x-app-layout>

@section('Titulo')
Artisen
@endsection





@section('contenido')

<!-- inicia carusel -->

<div class="row g-0 mt-4">
  <div class="col-lg-12 d-block">
  <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">

    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
      <div class="carousel-item active" data-bs-interval="10000">

        <div id="imagen">
        <img src="{{asset('adjuntos/banner1def.jpg')}}" class="d-block w-100 " alt="..." height="525px">
      </div>

        <div id="h33" class="carousel-caption d-block">
          <h3>Descuentos de Ropa</h3>
          <p>Las mejor ropa la mas perrona solo en Artisen.</p>
        </div>
      </div>
      <div class="carousel-item" data-bs-interval="2000">
        <img src="{{asset('adjuntos/banner2.jpg')}}" class="d-block w-100" alt="..."  height="525px">
        <div id="h33" class="carousel-caption d-block ">
          <h3>Increibles Descuentos de Ropa</h3>
          <p>¿Quieres ser cool? Compra en Artisen.</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{asset('adjuntos/banner3.jpg')}}" class="d-block w-100" alt="..." height="525px">
        <div id="h33" class="carousel-caption d-block">
          <h3>Artisen nya</h3>
          <p>¿Eres bobo? Compra en Artisen Baka.</p>
        </div>
      </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark"  data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Anterior</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark"  data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Siguiente</span>
    </button>
  </div>
  </div>
</div>
  


<div class="bg-dark p-2 mt-5"> 
  <h1 class="text-center m-0 text-light">Los más vendidos de Artisen</h1>
  </div>
<section class="store mt-5">
  <div class="container">
      <div class="items">
          <div class="row g-2 d-flex justify-content-center">
@php
$i = 0
@endphp
@foreach ($productos as $productos)   

@if ($i < 4)

    
<div class="card h-100 d-inline d-flex justify-content-center m-2" style="width: 16rem;" >
  <div class="item shadow">
    <img src="{{asset('storage/'.$productos->imagen)}}" class="item-image  " alt="..." width="250px" height="250px" >
    <div class="card-body ">
      <h5 style="display: none;"class="item-id text-center">{{$productos->id}} </h5>
      <h5 class="item-title text-center">{{$productos->nombre}} </h5>
      <h5 class="item-price text-center">${{$productos->precio}} </h5>
      <p class="card-text">{{$productos->descripcion}}</p>
      @if (Route::has('login'))
      @auth
      <a href="{{ route('catalogoF.index') }}" class="item-button btn btn-primary addToCart w-100">VER EN CATALOGO</a>
        @else
          <a href="{{ route('login') }}" class="item-button btn btn-primary addToCart w-100">VER EN CATALOGO</a>
      @endauth
      @endif
    </div>
  </div>
  </div>
  @php
     $i=$i+1
  @endphp

  @endif
@endforeach


</div>
</div>
</div>
</section>


@endsection

</x-app-layout>