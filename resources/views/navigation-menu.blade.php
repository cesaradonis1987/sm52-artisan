
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
      

        <a class="navbar-brand" href="#">
          <img src="/adjuntos/JjOgaq1jAYfgORbZIzOrAwcOhhxgWwwhIUvqnIg0.png" alt="" width="40" height="35" class="d-inline-block align-top">
          Artisen</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
          
        <form class="navbar-nav ms-5 mb-2 mb-lg-0">
          
          <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-light" type="submit">Search</button>
          
        </form>

        </div>
        

        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">

          <ul class="navbar-nav ms-auto mb-2 mb-lg-0">

            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Acerca</a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="#">Gestionar</a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Catalogo
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#">Ropa</a></li>
                <li><a class="dropdown-item" href="#">Figuras</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="#">Contacto</a></li>
              </ul>
            </li>
            <li class="nav-item">
              <!-- Dropdown usuariooooooooooooooooooooooooooooooooo -->
            @if (Route::has('login'))
            <div >
                @auth
                    
                        
                          
                            <div class="btn-group" role="group">
                              <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                {{ Auth::user()->name }}
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="btnGroupDrop1">


                                <li><a href="{{ url('/dashboard') }}" class="dropdown-item">Informacion</a></li>

                                <li><a href="{{ route('profile.show') }}" class="dropdown-item">Editar</a></li>

                                <li>
                                    <form method="POST" action="{{ route('logout') }}">
                                        @csrf

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                     onclick="event.preventDefault();
                                            this.closest('form').submit();">Salir</a></li>


                              </ul>
                            </form>
                            </div>
                          </div>
                    
                    
                @else
                    <a href="{{ route('login') }}" class="btn btn-primary">Ingresar</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="btn btn-primary">Registrar</a>
                    @endif
                @endauth
            </div>
        @endif



        <!-- Fin dropdown -->
            </li>
          </ul>
         
  

        </div>
      
      </div>
    </div>
    </nav>
