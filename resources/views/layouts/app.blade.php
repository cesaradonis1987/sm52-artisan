<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('Titulo')</title>
        <!-- Fuentes -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-vSIIfh2YWi9wW0r9iZe7RJPrKwp6bG+s9QZMoITbCckVJqGCCRhc+ccxNcdpHuYu" crossorigin="anonymous">
        <!-- Estilos -->
        <link rel="stylesheet" href="/css/cesar.css">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <link rel="stylesheet" href="/css/pascual.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <!-- Cosas de js -->
        <script src="{{ mix('js/app.js') }}" defer></script>
        @yield('css')


        
    </head>
    <body class="font-sans antialiased">

        <!-- Comienza el Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
      

        <a class="navbar-brand" href="{{ route('home') }}">
          <img src="/adjuntos/JjOgaq1jAYfgORbZIzOrAwcOhhxgWwwhIUvqnIg0.png" alt="" width="40" height="35" class="d-inline-block align-top">
          Artisen</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-end  d-none" id="navbarSupportedContent">
          
        <form class="navbar-nav ms-5 mb-2 mb-lg-0">
          
          <input class="form-control me-2" type="search" placeholder="Buscar..." aria-label="Search">
          <button class="btn btn-outline-light" type="submit">Buscar</button>
          
        </form>

        </div>
        

        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">

          <ul class="navbar-nav ms-auto mb-2 mb-lg-0">

            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="{{ route('home') }}">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="{{ route('acerca') }}">Acerca</a>
            </li>
            @if (Route::has('login'))
            @auth
            @if (Auth::user()->rol==='ADM')
            
            <li class="nav-item">
              <a class="nav-link active" href="{{ route('productos.index') }}">Gestionar</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="{{ route('ventas.index') }}">Ventas</a>
            </li>
            @endif
            @endauth
            @endif
            <li class="nav-item dropdown">
              <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Catalogo
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="{{ route('catalogoR.index') }}">Ropa</a></li>
                <li><a class="dropdown-item" href="{{ route('catalogoF.index') }}">Figuras</a></li>
                <li><hr class="dropdown-divider"></li>
              </ul>
            </li>
            <li class="nav-item">


              
              <!-- Dropdown del usuario -->
            @if (Route::has('login'))
            <div >
                @auth

                            <div class="btn-group" role="group">
                              <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                {{ Auth::user()->name }}
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="btnGroupDrop1">


                                <li><a href="{{ url('/dashboard') }}" class="dropdown-item">Informacion</a></li>

                                <li><a href="{{ route('profile.show') }}" class="dropdown-item">Editar</a></li>

                                <li>
                                    <form method="POST" action="{{ route('logout') }}">
                                        @csrf

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                     onclick="event.preventDefault();
                                            this.closest('form').submit();">Salir</a></li>


                              </ul>
                            </form>
                            </div>
                          </div>
                    
                    
                @else
                    <a href="{{ route('login') }}" class="btn btn-primary">Ingresar</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="btn btn-primary">Registrar</a>
                    @endif
                @endauth
            </div>
        @endif



        <!-- Fin dropdown -->
            </li>
          </ul>
         
  

        </div>
      
      </div>
    </div>
    </nav>

        


            <!-- Page Content -->


            <div class="container">
              <div>
              @yield('contenido')
              </div>
                </div>

            <main>
                {{ $slot }}
            </main>
        </div>
        @stack('modals')
        @livewireScripts










        


<!-- Page foter -->
<footer class="mt-5" id="footer">
    <div class="footer">
        <div class="contain">
        <div class="col">
          <h1>Contacto</h1>
          <ul>
            <li></li>
            
            <li><i class="fas fa-phone-square-alt fa-2x"></i> +52 998 3997 088 <p>Numero de la empresa</p></li>
           
            <li><i class="fas fa-map-marker-alt fa-2x"></i> Carretera Cancún-Aeropuerto, Km. 11.5 S.M. 299, Mz. 5, Lt 1, 77565 Cancùn, Q.R.</li>
            <li style="color:#0e7ebd;"><i class="icon-envelope icon-2x" style="color: white;"></i> cesaradonis1@hotmail.com</li>
          </ul>
        </div>
        
        
        
        
        <div class="col social">
          <h1>Acerca de la Compañia</h1>
          <ul>
            <li><p>Somos una empresa dedicada a la venta de accesorios y productos de indole "Anime" el cual se compromete a llevar productos de gran calidad a nuestros consumidores, ademas proporcionar lo mejor en calidad de manga, accesorios y cosplay.</p></li>
            <li><a href="https://www.facebook.com/profile.php?id=100009391373569"><i class="fab fa-facebook-square fa-5x"></i></a><a href="https://twitter.com/HazzaHotFake"> <i class="fab fa-twitter-square fa-5x"></i></a><a href="https://gitlab.com/cesaradonis1987"> <i class="fab fa-github-square fa-5x  "></i></a> <a href="https://www.instagram.com/vituz118/"> <i class="fab fa-instagram-square fa-5x  "></i></a></li>
          </ul>
        </div>
      <div class="clearfix"></div>
      </div>
      </div>
</footer>


<!-- fin foter -->

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

        @yield('js')
    </body>
</html>
