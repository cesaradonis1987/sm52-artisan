<x-app-layout>
    @section('Titulo')
Artisen
@endsection





@section('contenido')

<div class="container1 mt-1" >
<form action="{{ route('productos.update', $producto->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <h1 id="titulazo">Editar Producto</h1>

    
        <div class="input-group mb-3">
            <div class="titulo1">
            <p>Nombre del articulo</p>
            </div>
        <input type="text" aria-label="First name" class="form-control" name="nombre" value="{{ $producto->nombre}}" required>
        </div>
      


      
      <div class="input-group mb-3">
        <div class="titulo1">
            <p>Descripcion</p>
        </div>
        <textarea class="form-control" aria-label="With textarea" name="descripcion" required>{{$producto->descripcion}}</textarea>
      </div>
    

        <div class="input-group mb-3">
            <div class="titulo1">
                <p>Categoria</p>
                </div>
            <select class="form-select" id="inputGroupSelect01" name="categoria" required>
              <option selected>Ninguna</option>
              <option value="Ropa">Ropa</option>
              <option value="Figura">Figura</option>
            </select>
          </div>

          <div class="container-otro">
            <div class="row align-items-start">
              <div class="col">
                <div class="input-group mb-3">
                    <div class="titulo1">
                    <p>Precio</p>
                    </div>
                    <span class="input-group-text">$</span>
                    <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" name="precio" value="{{ $producto->precio}}" required>
                    <span class="input-group-text">.00</span>
                </div>
              </div>
              <div class="col">
                <div class="input-group mb-3">
                    <div class="titulo1">
                    <p>Cantidad</p>
                    </div>
                <input type="text" aria-label="First name" class="form-control" name="cantidad" value="{{ $producto->cantidad}}" required>
                <span class="input-group-text">Unidades</span>
                </div>
              </div>
            </div>
          </div>

            <div class="input-group mb-3">
                <div class="titulo1">
                <p>Foto del Producto</p>
                </div>
                <input type="file" class="form-control" id="inputGroupFile04" 
                aria-describedby="inputGroupFileAddon04" aria-label="Upload" name="imagen" value="{{ $producto->imagen}}" required>
            </div>

            <div class="input-group mb-3">
                <button type="submit" id="guardarboton">Guardar</button>
            </div>
      

</div>
</form>


@endsection
       
</x-app-layout>