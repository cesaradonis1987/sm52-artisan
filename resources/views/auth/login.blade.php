<x-guest-layout>
    <x-jet-authentication-card>
        

    <section>
        <div class="row g-0">
            <div class="col-lg-7 d-none d-lg-block">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item img-1 min-vh-100 active">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Lo mas vedido</h5>
                                <p>Camisa Miku</p>
                              </div>
                        </div>
                        <div class="carousel-item img-2 min-vh-100">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Artisen</h5>
                                <p>Lo mejor lo kawai lo encuentras en Artisen</p>
                              </div>
                        </div>
                        <div class="carousel-item img-3 min-vh-100">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Artisen</h5>
                                <p>Lo mejor lo kawai lo encuentras en Artisen</p>
                              </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

            <div class="col-lg-5 d-flex flex-column align-items-end min-vh-100">
                
                <div class="px-lg-5 pt-lg-4 pb-lg-3 p-4 w-100 mx-auto  d-flex justify-content-center">
                    <img src="{{asset('adjuntos/logo.svg')}}" alt="" class="img-fluid" width="100px" height="100px">
                </div>


                <div class="px-lg-5 py-lg-4  align-self-center ">
                    <h1 class="font-weight-bold  text-center">Bienvenido de vuelta</h1>


                    
                    <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }} " class="mb-4">
            @csrf

            <div>
                <x-jet-label for="email" value="{{ __('Correo') }}" class="mb-3" />
                <x-jet-input id="email" class="form-control bg-dark-x  border-0 mb-1" id="exampleInputEmail1" 
                type="email" name="email" :value="old('email')" placeholder="Ingresa tu email" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Contraseña') }}" />
                <x-jet-input id="password" class="form-control bg-dark-x  border-0 mb-1" 
                type="password" name="password" placeholder="Ingresa tu contraseña" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember" type="checkbox"  class="form-check-input"/>
                    <span class="ml-2 text-sm text-gray-600">{{ __('Recuerdame') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-jet-button class="btn btn-primary w-100">
                    {{ __('Iniciar Sesion') }}
                </x-jet-button>


                
            
            </div>
        </form>

                    

                <div class="text-center px-lg-5 pt-lg-3 pb-lg-4  w-100 mt-auto">
                    <p class="d-inline-block mb-0">¿Todavia no tienes una cuenta?</p> <a href="{{ route('register') }}" class="text-light font-weight-bold text-decoration-none">Crear una ahora</a>
                </div>

            </div>
        </div>
    </section>



    </x-jet-authentication-card>
</x-guest-layout>
