<x-app-layout>

    


    @section('Titulo')
Artisen
@endsection


@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">   
@endsection


@section('contenido')

<div class="bg-dark p-3 mt-4"> 
    <h1 class="text-center m-0 text-light">VENTAS REALIZADAS</h1>
    </div>

<table id="usuarios" class="table table-striped mt-4" style="width:100%">
    <thead class="table-dark">
        <tr>
          <th scope="col">Articulo</th>
          <th scope="col">cliente</th>
          <th scope="col">Cantidad</th>
          <th scope="col">Costo</th>
          <th scope="col">Fecha</th>
        </tr>
    </thead>

    @if (Route::has('login'))
    @auth

  
  <tbody>   
    @foreach ($ventas as $ventas)
      <tr>
          <td>{{ $ventas->productos }}</td>
          <td>{{ $ventas->clientenomb }}</td>
          <td>{{ $ventas->cantidad }}</td>
          <td>{{ $ventas->coste }}</td>
          <td>{{ $ventas->updated_at }}</td>
        
      </tr> 
  @endforeach

  @endauth
      @endif
    </tbody>

    <tfoot class="table-dark">
        <tr>
            <th scope="col">Articulo</th>
            <th scope="col">cliente</th>
            <th scope="col">Cantidad</th>
            <th scope="col">Costo</th>
            <th scope="col">Fecha</th>
          </tr>
    </tfoot>
</table>


@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js"></script>
<script>$('#usuarios').DataTable();</script>
@endsection



@endsection            
</x-app-layout>
