<x-app-layout>

    


    @section('Titulo')
Artisen
@endsection





@section('contenido')


<div class="bg-dark p-3 mt-4"> 
    <h1 class="text-center m-0 text-light">Artisen</h1>
    </div>

<div class="container-xl mt-3" >
    <div class="card text-center" style="box-shadow: 0px 10px 10px black;">
        <div class="card-header">
          Detalles de usuario
        </div>
        <div class="card-body">
            <div class="row g-2">
                
                <div class="col-sm-4 " >
                    <div class="d-flex justify-content-center">
                    <img src="{{asset('adjuntos/avatar.jpg')}}" class="" style="border-radius: 50%;" width="200px" height="200px">
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">{{Auth::user()->name}}</li>
                    </ul>
                </div>
                <div class="col-sm-8" >
                    <ul class="list-group list-group-horizontal-xxl" >
                        <li class="list-group-item">ID: {{Auth::user()->id}}</li>
                        <li class="list-group-item">Correo: {{Auth::user()->email}}</li>
                        <li class="list-group-item">Rol: {{Auth::user()->rol}}</li>
                        <li class="list-group-item">Hola el equipo de Artisen te ama guap@.</li>

                      </ul>
                </div>
              </div>
        </div>
        <div class="card-footer text-muted">
            Fecha de creacion: {{Auth::user()->created_at}}
        </div>
      </div>
</div>



</div>
<footer class="bg-dark p-3 mt-5">
    <p class="text-center m-0 text-light">COMPRAS REALIZADAS</p>
  </footer>

  <table class="table" id="data" class="display">
    <thead class="table-dark">
        <tr>
          <th scope="col">Articulo</th>
          <th scope="col">Cantidad</th>
          <th scope="col">Costo</th>
          <th scope="col">Fecha</th>
        </tr>
      </thead>


      @if (Route::has('login'))
      @auth
    @foreach ($ventas as $ventas)
    <tbody>   
        @if (Auth::user()->name===$ventas->clientenomb)
        <tr>
            <td>{{ $ventas->productos }}</td>
            <td>{{ $ventas->cantidad }}</td>
            <td>{{ $ventas->coste }}</td>
            <td>{{ $ventas->updated_at }}</td>
          
        </tr>
        @else
        
        @endif
        
    @endforeach

    @endauth
        @endif
</table>

@endsection








            
</x-app-layout>
