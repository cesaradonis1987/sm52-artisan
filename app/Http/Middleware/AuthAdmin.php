<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {


        if($request->user()->rol === 'USR'){

            abort(403, "¡No tienes el permiso para ver esto estas chikito! le diremos a tus padres.");}

        
        return $next($request);
    }
    


}
