<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\CatalogoFigurasController;
use App\Http\Controllers\CatalogoRopaController;
use App\Http\Controllers\VentasController;
use App\Models\Ventas;
use App\Models\Productos;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $productos=Productos::all()->where('categoria', 'Figura');
    return view('home',compact('productos'));
})->name('home');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {$ventas=Ventas::all();
    return view('dashboard', compact('ventas'));
})->name('dashboard');

Route::view('acerca', 'acerca')->name('acerca');

Route::view('contacto','contac')->name('contac');

Route::view('prueba','prueba')->name('prueba');

/*Route::view('contacto', function () { $ventas2=Ventas::all();
    return view('ventas', compact('ventas2'));
})->name('dashboard');*/
Route::resource('catalogoF', CatalogoFigurasController::class);
Route::resource('catalogoR', CatalogoRopaController::class);
Route::resource('ventas', VentasController::class);
Route::middleware(['auth:sanctum', 'verified' , 'authadmin'])->resource('productos', ProductosController::class);